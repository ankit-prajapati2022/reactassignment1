import React from 'react'

function LapRecords(props) {
    return (
        <div className='m-4'>
            <h3>Lap Records</h3>
            <ol>
                {
                    props.lapArr.map((lap, index) => {
                        return <li key={index}>{("0" + Math.floor((lap / 60000) % 60)).slice(-2)}:{("0" + Math.floor((lap / 1000) % 60)).slice(-2)}.{("0" + ((lap / 10) % 100)).slice(-2)}
                        </li>
                    })
                }
            </ol>
        </div>
    )
}

export default LapRecords