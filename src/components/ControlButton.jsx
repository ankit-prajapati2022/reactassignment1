import React from "react";

export default function ControlButtons(props) {
  const StartButton = (
    <div className="btng btn-one btn-start"
      onClick={props.handleStart}>
      Start
    </div>
  );

  const ActiveButtons = (
    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
      <button type="button" class="btn btn-success" onClick={() => { props.setLapArr(ti => [...ti, props.time]) }}>Lap</button>
      <button type="button" class="btn btn-warning" onClick={props.handlePauseResume}>{props.isPaused ? "Resume" : "Pause"}</button>
      <button type="button" class="btn btn-danger" onClick={props.isPaused && props.handleReset}>Reset</button>
    </div>
  );

  return (
    <>
      <div className="Control-Buttons ms-3">
        <div>{props.active ? ActiveButtons : StartButton}</div>
        <br />
        <div className="btng btn-one" onClick={() => { props.setLapArr([]) }}>Stop Watch</div>
      </div>
    </>
  );
}