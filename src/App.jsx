import React from 'react'
import StopWatch from './components/StopWatch'
import LapRecords from './components/LapRecords'

function App() {

  const [lapArr, setLapArr] = React.useState([])

  return (
    <div className='container my-5'> 
      <div className="row justify-content-around">
        <div className="col-lg-4 my-3">
      <StopWatch setLapArr={setLapArr} />
        </div>
        <div className="col-lg-6">
          <LapRecords lapArr={lapArr} />
        </div>
      </div>
    </div>
  )
}

export default App
